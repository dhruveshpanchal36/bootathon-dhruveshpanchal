function generateTable() {
    var no = document.getElementById("no");
    var tab = document.getElementById("table");
    //Exception Handling
    if (isNaN(parseInt(no.value))) {
        alert("Enter A No");
        no.value = " ";
        return;
    }
    var i;
    //Deleting rows when new input is given
    while (tab.rows.length > 0) {
        tab.deleteRow(0);
    }
    var row = tab.insertRow();
    // creating a cell and adding data to it
    // header part of the table
    var cell = row.insertCell();
    cell.innerHTML = 'Multiplcation';
    var cell = row.insertCell();
    cell.innerHTML = 'Answer';
    // Adding rows to the table
    for (i = 1; i <= parseInt(no.value); i++) {
        var row = tab.insertRow();
        var cell = row.insertCell();
        cell.innerHTML = no.value + " * " + i.toString();
        var cell = row.insertCell();
        cell.innerHTML = (parseInt(no.value) * i).toString();
    }
}
//# sourceMappingURL=backend.js.map