// Initialization of variables from input
var x1 = document.getElementById("x1");
var y1 = document.getElementById("y1");
var x2 = document.getElementById("x2");
var y2 = document.getElementById("y2");
var x3 = document.getElementById("x3");
var y3 = document.getElementById("y3");
var c1 = document.getElementById("c1");
var c2 = document.getElementById("c2");
var ans = document.getElementById("finalAns");
var listOfPoints = [x1, y1, x2, y2, x3, y3, c1, c2];
// Main proccessing is done here
function getData() {
    var thirdPoint; // holds value of equation of side with x and y of remaing side
    var givenPoint; // holds value of equation of side with x and y of desired point
    if (checkForInvalidInput() == false)
        return;
    if (checkForSamePoints() == true) {
        alert("Given 2 Points Out of 3 Are Same, Triangle Can't be formed");
        return;
    }
    if (checkForColinearPoints() == true) {
        alert("Given Points Are Co-Linear Triangle Can't be formed");
        return;
    }
    // considering side 1
    thirdPoint = calculateValueOfLineFunction(parseFloat(x1.value), parseFloat(y1.value), parseFloat(x2.value), parseFloat(y2.value), parseFloat(x3.value), parseFloat(y3.value), 1);
    givenPoint = calculateValueOfLineFunction(parseFloat(x1.value), parseFloat(y1.value), parseFloat(x2.value), parseFloat(y2.value), parseFloat(c1.value), parseFloat(c2.value), 0);
    if (thirdPoint != givenPoint) {
        if (givenPoint == -1) {
            ans.innerHTML = "Point Lies On Triangle";
            return;
        }
        ans.innerHTML = "Point Lies Outside Triangle";
        return;
    }
    thirdPoint = calculateValueOfLineFunction(parseFloat(x1.value), parseFloat(y1.value), parseFloat(x3.value), parseFloat(y3.value), parseFloat(x2.value), parseFloat(y2.value), 1);
    givenPoint = calculateValueOfLineFunction(parseFloat(x1.value), parseFloat(y1.value), parseFloat(x3.value), parseFloat(y3.value), parseFloat(c1.value), parseFloat(c2.value), 0);
    if (thirdPoint != givenPoint) {
        if (givenPoint == -1) {
            ans.innerHTML = "Point Lies On Triangle";
            return;
        }
        ans.innerHTML = "Point Lies Outside Triangle";
        return;
    }
    thirdPoint = calculateValueOfLineFunction(parseFloat(x2.value), parseFloat(y2.value), parseFloat(x3.value), parseFloat(y3.value), parseFloat(x1.value), parseFloat(y1.value), 1);
    givenPoint = calculateValueOfLineFunction(parseFloat(x2.value), parseFloat(y2.value), parseFloat(x3.value), parseFloat(y3.value), parseFloat(c1.value), parseFloat(c2.value), 0);
    if (thirdPoint != givenPoint) {
        if (givenPoint == -1) {
            ans.innerHTML = "Point Lies On Triangle";
            return;
        }
        ans.innerHTML = "Point Lies Outside Triangle";
        return;
    }
    ans.innerHTML = "Point Lies Inside Triangle";
}
// This function is invoked when the given co-ordinates satisfies the eqn of line(side of traingle) and to check whether
//its in the range of the side of the trinagle  
// 1-4 parameters are co-ordinates of line(side of triangle) last two are arbitary point co-ordinates for checking 
function inRange(x1, y1, x2, y2, x3, y3) {
    var num;
    if (x1 > x2) {
        num = x1;
        x1 = x2;
        x2 = num;
    }
    if (y1 > y2) {
        num = y1;
        y1 = y2;
        y2 = num;
    }
    if (x3 >= x1 && x3 <= x2 && y3 >= y1 && y3 <= y2)
        return true;
    return false;
}
//This function is used to evaluate the eqn of line at x=x3 and y=y3
// flag is used to indicate that the co-ordinates are of point which needs to be checked
function calculateValueOfLineFunction(x1, y1, x2, y2, x3, y3, flag) {
    if (x1 == x2) {
        if (x1 == x3 && flag == 0) {
            if (inRange(x1, y1, x2, y2, x3, y3) == true)
                return -1;
        }
        else if (x1 < x3)
            return 1;
        else
            return 0;
    }
    if (y1 == y2) {
        if (y1 == y3 && flag == 0) {
            if (inRange(x1, y1, x2, y2, x3, y3) == true)
                return -1;
        }
        else if (y1 < y3)
            return 1;
        else
            return 0;
    }
    var z = (x3 - x1) / (x1 - x2) - (y3 - y1) / (y1 - y2);
    if (z > 0)
        return 1;
    else if (z == 0) {
        if (inRange(x1, y1, x2, y2, x3, y3) == true)
            return -1;
    }
    else
        return 0;
}
//Check is given points have same co-ordinates if so, then, triangle cannot be formed 
function checkForSamePoints() {
    if ((x1.value == x2.value && y1.value == y2.value) || (x1.value == x3.value && y1.value == y3.value) || (x2.value == x3.value && y2.value == y3.value))
        return true;
    return false;
}
// checks if given points are colinear or not
function checkForColinearPoints() {
    var a = parseFloat(x1.value) - parseFloat(y1.value);
    var b = parseFloat(x2.value) - parseFloat(y2.value);
    var c = parseFloat(x3.value) - parseFloat(y3.value);
    if ((x1.value == x2.value && x2.value == x3.value) || (y1.value == y2.value && y2.value == y3.value) || (a == 0 && b == 0 && c == 0))
        return true;
    return false;
}
// checks for valid input in textbox
function checkForInvalidInput() {
    var i;
    for (i = 0; i < listOfPoints.length; i++) {
        if (isNaN(parseFloat(listOfPoints[i].value))) {
            alert("Enter A valid Input at " + listOfPoints[i].id);
            return false;
        }
    }
    return true;
}
/*
    Algorithm Of The Code

    1)Get Valid Co-ordinates of triangle
    2)for each side of triangle
    3)  a = side.equation(remaing side x,y)
    4)  b = side.equation(mentioned point x,y)
    5)  if a and b both +ve or -ve then
    6)      continue
    7)  else
    8)      break (point outside of triangle)


*/ 
//# sourceMappingURL=backend2.js.map