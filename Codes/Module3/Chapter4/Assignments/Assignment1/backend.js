var ans = document.getElementById("ans");
var noOfRows = document.getElementById("noforces");
var tab = document.getElementById("table");
var inputlist = [];
var forces = [];
var angle = [];
//After input is given the data of forces & angle array is feed with this function
function getData() {
    for (let i = 0; i < parseInt(noOfRows.value); i++) {
        for (let j = 0; j < 2; j++) {
            if (j == 0) {
                var ele = document.getElementById("f" + i + j);
                forces.push(parseInt(ele.value));
            }
            else {
                var ele = document.getElementById("a" + i + j);
                angle.push(parseInt(ele.value));
            }
        }
    }
}
//creates new table and adds new rows to it
function createTable(tab) {
    var row = parseInt(noOfRows.value);
    var col = 2;
    var tab_row = tab.insertRow();
    var tab_cell = tab_row.insertCell();
    tab_cell.innerHTML = "Forces";
    var tab_cell = tab_row.insertCell();
    tab_cell.innerHTML = "Angles";
    for (let i = 0; i < row; i++) {
        var tab_row = tab.insertRow();
        for (let j = 0; j < col; j++) {
            var tab_cell = tab_row.insertCell();
            var ele = document.createElement("input");
            if (j == 0) {
                ele.id = "f" + i + j;
                ele.placeholder = "Force " + (i + 1);
            }
            else {
                ele.id = "a" + i + j;
                ele.placeholder = "Angle " + (i + 1);
            }
            ele.type = "text";
            tab_cell.appendChild(ele);
            inputlist.push(ele);
        }
    }
    for (let i = 0; i < inputlist.length; i++) {
        console.log(inputlist[i]);
    }
}
//deletes the pre-existing table and arrays containg data of previous table
function deleteTable(tab) {
    inputlist.length = 0;
    forces.length = 0;
    angle.length = 0;
    ans.innerHTML = "Ans";
    while (tab.rows.length > 0)
        tab.deleteRow(0);
}
//Onchange of textbox new table is generated
function generateTable() {
    var row = parseInt(noOfRows.value);
    if (isNaN(row) || row <= 0) {
        return;
    }
    deleteTable(tab);
    createTable(tab);
}
//Main Processing is done here
function getResultant() {
    if (checkForInvalidInput() == false)
        return;
    getData();
    var fx = 0;
    var fy = 0;
    var res;
    var rangle;
    for (let i = 0; i < forces.length; i++) {
        fy += forces[i] * Math.sin(angle[i] * Math.PI / 180);
        fx += forces[i] * Math.cos(angle[i] * Math.PI / 180);
    }
    console.log(fx + "<br>" + fy);
    console.log(fy / fx);
    res = Math.sqrt(Math.pow(fx, 2) + Math.pow(fy, 2));
    rangle = Math.atan2(fy, fx) * 180 / Math.PI;
    ans.innerHTML = " Resultant = " + res.toFixed(3) + "<br>" + "Angle = " + rangle + "     (in Degree)";
    forces.length = 0;
    angle.length = 0;
}
// checks for valid input in textbox
function checkForInvalidInput() {
    var i;
    for (i = 0; i < inputlist.length; i++) {
        if (isNaN(parseInt(inputlist[i].value))) {
            if (inputlist[i].id.substring(0, 1) == "f")
                alert("Enter A valid Input at Force " + (parseInt(inputlist[i].id.substring(1, 2)) + 1));
            else
                alert("Enter A valid Input at Angle " + inputlist[i].id.substring(1, 2));
            return false;
        }
    }
    return true;
}
//# sourceMappingURL=backend.js.map