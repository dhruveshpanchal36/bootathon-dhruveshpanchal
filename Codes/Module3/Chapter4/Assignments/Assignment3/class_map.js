function changeClass() {
    var canvas = document.getElementById("mycanvas");
    var context = canvas.getContext("2d");
    context.clearRect(0, 0, canvas.width, canvas.height);
    var x_inp = document.getElementById("x");
    var y_inp = document.getElementById("y");
    let and = new And(x_inp, y_inp, canvas, context);
    and.draw();
    var x_inp2 = document.getElementById("x2");
    var y_inp2 = document.getElementById("y2");
    let not = new Not(x_inp2, y_inp2, canvas, context);
    not.draw();
}
//# sourceMappingURL=class_map.js.map