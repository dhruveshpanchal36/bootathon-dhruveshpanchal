class Not {
    constructor(x, y, canvas, context) {
        this.x = x;
        this.y = y;
        this.canvas = canvas;
        this.context = context;
    }
    draw() {
        var x = parseInt(this.x.value);
        var y = parseInt(this.y.value);
        if (isNaN(x) || isNaN(y) || x <= 0 || y <= 0)
            return;
        //Draw Line
        this.context.beginPath();
        this.context.moveTo(x, y - 60);
        this.context.strokeStyle = "blue";
        this.context.lineTo(x, y + 60);
        this.context.lineWidth = 2;
        this.context.stroke();
        this.context.beginPath();
        this.context.moveTo(x, y - 60);
        this.context.lineTo(x + 60, y);
        this.context.stroke();
        this.context.beginPath();
        this.context.moveTo(x, y + 60);
        this.context.lineTo(x + 60, y);
        this.context.stroke();
        this.context.beginPath();
        this.context.arc(x + 60 + 5, y, 5, 0, 2 * Math.PI);
        this.context.stroke();
        //Output Line
        this.context.beginPath();
        this.context.moveTo(x + 70, y);
        this.context.lineTo(x + 80 + 60, y);
        this.context.stroke();
        //Input Lines
        this.context.beginPath();
        this.context.moveTo(x, y - 30);
        this.context.lineTo(x - 80, y - 30);
        this.context.stroke();
        this.context.beginPath();
        this.context.moveTo(x, y + 30);
        this.context.lineTo(x - 80, y + 30);
        this.context.stroke();
    }
}
class And {
    constructor(x, y, canvas, context) {
        this.x = x;
        this.y = y;
        this.canvas = canvas;
        this.context = context;
    }
    draw() {
        var x = parseInt(this.x.value);
        var y = parseInt(this.y.value);
        if (isNaN(x) || isNaN(y) || x <= 0 || y <= 0)
            return;
        //Draw Line
        this.context.beginPath();
        this.context.moveTo(x, y - 60);
        this.context.strokeStyle = "blue";
        this.context.lineTo(x, y + 60);
        this.context.lineWidth = 2;
        this.context.stroke();
        //Draw Semicircle
        this.context.beginPath();
        this.context.arc(x, y, 60, 0, Math.PI / 2);
        this.context.strokeStyle = "blue";
        this.context.stroke();
        this.context.beginPath();
        this.context.arc(x, y, 60, -Math.PI / 2, 0);
        this.context.strokeStyle = "blue";
        this.context.stroke();
        //Output Line
        this.context.beginPath();
        this.context.moveTo(x + 60, y);
        this.context.lineTo(x + 80 + 60, y);
        this.context.stroke();
        //Input Lines
        this.context.beginPath();
        this.context.moveTo(x, y - 30);
        this.context.lineTo(x - 80, y - 30);
        this.context.stroke();
        this.context.beginPath();
        this.context.moveTo(x, y + 30);
        this.context.lineTo(x - 80, y + 30);
        this.context.stroke();
    }
}
//# sourceMappingURL=backend.js.map