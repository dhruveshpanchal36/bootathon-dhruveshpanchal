function changeClass() {

    var canvas:HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
    var context:CanvasRenderingContext2D = canvas.getContext("2d");
    context.clearRect(0, 0,canvas.width, canvas.height );

    var x_inp:HTMLInputElement = <HTMLInputElement>document.getElementById("x");
    var y_inp:HTMLInputElement = <HTMLInputElement>document.getElementById("y");

    let and:And = new And(x_inp, y_inp, canvas, context);
    and.draw();

    var x_inp2:HTMLInputElement = <HTMLInputElement>document.getElementById("x2");
    var y_inp2:HTMLInputElement = <HTMLInputElement>document.getElementById("y2");

    let not :Not = new Not(x_inp2, y_inp2, canvas, context);
    not.draw();

}