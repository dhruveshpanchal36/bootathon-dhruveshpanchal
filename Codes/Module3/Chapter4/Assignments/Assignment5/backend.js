// canvas initialization
class Pendulum {
    constructor(x, y, strlen, radius, canvas, context) {
        this.x = x;
        this.y = y;
        this.strlen = strlen;
        this.radius = radius;
        this.canvas = canvas;
        this.context = context;
    }
    draw() {
        var x = parseInt(this.x.value);
        var y = parseInt(this.y.value);
        var strlen = parseInt(this.strlen.value);
        var radius = parseInt(this.radius.value);
        if (isNaN(x) || isNaN(y) || isNaN(strlen) || isNaN(radius) || x <= 0 || y <= 0 || strlen <= 0 || radius <= 0)
            return;
        //String
        this.context.beginPath();
        this.context.moveTo(x, y);
        this.context.strokeStyle = "blue";
        this.context.lineTo(x, y + strlen);
        this.context.lineWidth = 2;
        this.context.stroke();
        //circle
        this.context.beginPath();
        this.context.arc(x, y + strlen + radius, radius, 0, 2 * Math.PI);
        this.context.fillStyle = "red";
        this.context.fill();
        this.context.stroke();
        //support
        this.context.beginPath();
        this.context.moveTo(x, y);
        this.context.strokeStyle = "black";
        this.context.lineTo(x + 60, y);
        this.context.lineTo(x - 60, y);
        this.context.stroke();
    }
}
class Waves {
    constructor(x, y, amp, canvas, context) {
        this.x = x;
        this.y = y;
        this.amp = amp;
        this.canvas = canvas;
        this.context = context;
    }
    draw() {
        var x = parseInt(this.x.value);
        var y = parseInt(this.y.value);
        var amp = parseInt(this.amp.value);
        if (isNaN(x) || isNaN(y) || isNaN(amp) || x <= 0 || y <= 0 || amp <= 0)
            return;
        //Sine Wave
        this.context.beginPath();
        this.context.moveTo(x, y);
        this.context.strokeStyle = "blue";
        for (var i = 0; i <= 360; i++) {
            this.context.lineTo(x + i, y + amp * Math.sin(i * Math.PI / 180));
        }
        this.context.stroke();
        //Cos Wave
        this.context.beginPath();
        this.context.moveTo(x, y);
        this.context.strokeStyle = 'red';
        for (var i = 0; i <= 360; i++) {
            this.context.lineTo(x + i, y + amp * Math.cos(i * Math.PI / 180));
        }
        this.context.stroke();
        //X & Y axes
        this.context.beginPath();
        this.context.moveTo(x, y);
        this.context.lineTo(x + 360, y);
        this.context.strokeStyle = "black";
        this.context.lineWidth = 2;
        this.context.stroke();
        this.context.beginPath();
        this.context.moveTo(x, y);
        this.context.lineTo(x, y - amp);
        this.context.strokeStyle = "black";
        this.context.lineWidth = 2;
        this.context.stroke();
        this.context.beginPath();
        this.context.moveTo(x, y);
        this.context.lineTo(x, y + amp);
        this.context.strokeStyle = "black";
        this.context.lineWidth = 2;
        this.context.stroke();
        this.context.beginPath();
        this.context.moveTo(x, y);
        this.context.lineTo(x - amp, y);
        this.context.strokeStyle = "black";
        this.context.lineWidth = 2;
        this.context.stroke();
    }
}
//# sourceMappingURL=backend.js.map