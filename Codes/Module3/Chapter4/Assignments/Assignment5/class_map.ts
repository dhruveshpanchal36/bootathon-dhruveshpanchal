

function changeClass() {

    var canvas:HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
    var context:CanvasRenderingContext2D = canvas.getContext("2d");
    
    context.clearRect(0, 0,canvas.width, canvas.height );
    
    var x_inp:HTMLInputElement = <HTMLInputElement>document.getElementById("x");
    var y_inp:HTMLInputElement = <HTMLInputElement>document.getElementById("y");
    var amp_inp:HTMLInputElement = <HTMLInputElement>document.getElementById("amp");

    let c:Waves = new Waves(x_inp, y_inp, amp_inp, canvas, context );
    c.draw();
   

    var x_inp2:HTMLInputElement = <HTMLInputElement>document.getElementById("x2");
    var y_inp2:HTMLInputElement = <HTMLInputElement>document.getElementById("y2");
    var strlen_inp:HTMLInputElement = <HTMLInputElement>document.getElementById("strlen");
    var radius_inp:HTMLInputElement = <HTMLInputElement>document.getElementById("radius");

    let p:Pendulum = new Pendulum(x_inp2, y_inp2, strlen_inp,radius_inp, canvas, context );
    p.draw();
   
}

