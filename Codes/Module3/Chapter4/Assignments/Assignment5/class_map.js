function changeClass() {
    var canvas = document.getElementById("mycanvas");
    var context = canvas.getContext("2d");
    context.clearRect(0, 0, canvas.width, canvas.height);
    var x_inp = document.getElementById("x");
    var y_inp = document.getElementById("y");
    var amp_inp = document.getElementById("amp");
    let c = new Waves(x_inp, y_inp, amp_inp, canvas, context);
    c.draw();
    var x_inp2 = document.getElementById("x2");
    var y_inp2 = document.getElementById("y2");
    var strlen_inp = document.getElementById("strlen");
    var radius_inp = document.getElementById("radius");
    let p = new Pendulum(x_inp2, y_inp2, strlen_inp, radius_inp, canvas, context);
    p.draw();
}
//# sourceMappingURL=class_map.js.map