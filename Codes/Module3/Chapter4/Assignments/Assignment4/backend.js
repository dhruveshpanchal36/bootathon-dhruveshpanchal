class Dial {
    constructor(x, canvas, context) {
        this.circleDia = 200;
        this.x = x;
        this.canvas = canvas;
        this.context = context;
    }
    draw() {
        var deg_x = parseInt(this.x.value);
        var x = parseInt(this.x.value) * Math.PI / 180;
        console.log(x);
        if (isNaN(x))
            return;
        var px = Math.cos(-x) * (this.circleDia - 10) + this.canvas.width / 2;
        var py = Math.sin(-x) * (this.circleDia - 10) + this.canvas.height / 2;
        console.log(px);
        console.log(py);
        //Dial
        this.context.beginPath();
        this.context.strokeStyle = "blue";
        this.context.arc(this.canvas.width / 2, this.canvas.height / 2, this.circleDia, 0, 2 * Math.PI);
        this.context.stroke();
        //Center point
        this.context.beginPath();
        this.context.arc(this.canvas.width / 2, this.canvas.height / 2, 10, 0, 2 * Math.PI);
        this.context.fillStyle = "red";
        this.context.fill();
        this.context.stroke();
        //Draw Pointing Line
        this.context.beginPath();
        this.context.moveTo(this.canvas.width / 2, this.canvas.height / 2);
        this.context.lineTo(px, py);
        this.context.stroke();
        //Draw Small cicle
        var px = Math.cos(-x) * (this.circleDia - 5) + this.canvas.width / 2;
        var py = Math.sin(-x) * (this.circleDia - 5) + this.canvas.height / 2;
        this.context.beginPath();
        this.context.arc(px, py, 5, 0, Math.PI * 2);
        this.context.fillStyle = "red";
        this.context.fill();
        this.context.stroke();
        //show angle
        this.context.beginPath();
        this.context.arc(this.canvas.width / 2, this.canvas.height / 2, 50, -x, 0);
        this.context.stroke();
        //axes
        this.context.beginPath();
        this.context.strokeStyle = "black";
        this.context.moveTo(this.canvas.width / 2, this.canvas.height / 2);
        this.context.lineTo(this.canvas.width / 2, this.canvas.height / 2 + 90);
        this.context.moveTo(this.canvas.width / 2, this.canvas.height / 2);
        this.context.lineTo(this.canvas.width / 2, this.canvas.height / 2 - 90);
        this.context.moveTo(this.canvas.width / 2, this.canvas.height / 2);
        this.context.lineTo(this.canvas.width / 2 + 90, this.canvas.height / 2);
        this.context.moveTo(this.canvas.width / 2, this.canvas.height / 2);
        this.context.lineTo(this.canvas.width / 2 - 90, this.canvas.height / 2);
        this.context.stroke();
        //Showing Angle
        var px = Math.cos(-x / 2) * 65 + this.canvas.width / 2;
        var py = Math.sin(-x / 2) * 65 + this.canvas.height / 2;
        this.context.beginPath();
        this.context.font = "20px Arial";
        this.context.strokeText(deg_x.toString(), px, py);
    }
}
//# sourceMappingURL=backend.js.map