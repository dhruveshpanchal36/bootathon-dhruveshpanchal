function changeClass() {
    var canvas = document.getElementById("mycanvas");
    var context = canvas.getContext("2d");
    context.clearRect(0, 0, canvas.width, canvas.height);
    var x_inp = document.getElementById("x");
    let dial = new Dial(x_inp, canvas, context);
    dial.draw();
}
//# sourceMappingURL=class_map.js.map