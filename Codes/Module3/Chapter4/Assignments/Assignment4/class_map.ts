function changeClass() {

    var canvas:HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
    var context:CanvasRenderingContext2D = canvas.getContext("2d");
    context.clearRect(0, 0,canvas.width, canvas.height );

    var x_inp:HTMLInputElement = <HTMLInputElement>document.getElementById("x");
   
    let dial:Dial = new Dial(x_inp, canvas, context);
    dial.draw();

}