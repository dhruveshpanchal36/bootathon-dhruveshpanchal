var no1 = document.getElementById("no1"); // first no variable
var no2 = document.getElementById("no2"); // second no variable
var ans = document.getElementById("ans"); // final ans  variable
function getAns(operation) {
    if (isNaN(parseInt(no1.value)) || isNaN(parseInt(no2.value))) { // Error handling 
        alert("Please Enter A Number");
        if (isNaN(parseInt(no1.value)) && isNaN(parseInt(no2.value))) {
            no1.value = " ";
            no2.value = " ";
        }
        else if (isNaN(parseInt(no2.value))) {
            no2.value = " ";
        }
        else {
            no1.value = " ";
        }
    }
    else { // what to do when no error in input
        switch (operation) {
            case 'add':
                var c = parseInt(no1.value) + parseInt(no2.value);
                ans.value = c.toString();
                break;
            case 'sub':
                var c = parseInt(no1.value) - parseInt(no2.value);
                ans.value = c.toString();
                break;
            case 'mul':
                var c = parseInt(no1.value) * parseInt(no2.value);
                ans.value = c.toString();
                break;
            case 'div':
                var c = parseInt(no1.value) / parseInt(no2.value);
                ans.value = c.toString();
                break;
        }
    }
}
//# sourceMappingURL=backend.js.map