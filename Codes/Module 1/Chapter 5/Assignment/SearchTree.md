# Search Trees

### What is a Tree?
> In Computer Science Tree is a Non Linear Data Structure which has collection of nodes with designated node called the root node from which tree originates.

### What is a Search Tree?
> A search tree is a tree data structure used for locating specific keys from within a set.
> The key for each node must be greater than any keys in subtrees on the left, and less than any keys in subtrees on the right.

### Different Types Of Search Trees
* #### B-Tree
    > It is a tree in which no node can have more than two children. **or** It's a collection of nodes which can be categorized into three disjoint subsets  root, left subtree &
        right subtree which themself must be binary tree.
* #### Ternary Tree
    > A ternary search tree is a type of tree that can not have more than 3 nodes. 
    Each node stores a single character and the tree itself is ordered the same way a binary search tree is, with the exception of a possible third node.
* #### (a, b) Tree
    > An (a,b)-tree is a search tree where all of its leaves are the same depth. Each node has at least a children and at most b children, while the root has at least 2 children and at most b children.
    a and b can be decided with the following formula:
    > #### 2 <= a <= (b+1)/2

### Operation On Search Tree

* ### Searching Rerecursive
   search-recursive(key, node) <br>
    * if node is NULL <br>
        * return EMPTY_TREE <br>
    * if key < node.key <br>
        * return search-recursive(key, node.left) <br>
    * else if key > node.key <br>
        * return search-recursive(key, node.right) <br>
    * else <br>
        * return node <br>

* ### Searching Iterative
    * searchIterative(key, node)
    * currentNode := node
    * while currentNode is not NULL
        * if currentNode.key = key
            * return currentNode
        * else if currentNode.key > key
            * currentNode := currentNode.left
        * else
            * currentNode := currentNode.right

* ### Min Max
    > #### Min
    * findMinimum(node)
    * if node is NULL
        * return EMPTY_TREE
    * min := node
    * while min.left is not NULL
        * min := min.left
    * return min.key

    > #### Max
    * findMaximum(node)
        * if node is NULL
            * return EMPTY_TREE
        * max := node
        * while max.right is not NULL
            * max := max.right
        * return max.key

